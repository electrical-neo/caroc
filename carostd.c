#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

typedef uint8_t u8;
typedef int8_t i8;
typedef uint16_t u16;
typedef int16_t i16;
typedef uint32_t u32;
typedef int32_t i32;
typedef uint64_t u64;
typedef int64_t i64;

typedef struct {
	char *chars;
	u64 len;
} CaroStr;

void print_str(CaroStr x) {
	printf("%.*s", x.len, x.chars);
}

void print_i64(i64 x) {
	printf("%ld", x);
}

void print_bool(bool x) {
	printf("%s", x ? "true" : "false");
}

i64 scan_i64(CaroStr prompt) {
	print_str(prompt);
	i64 result;
	scanf("%ld", &result);
	return result;
}
