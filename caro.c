#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <llvm-c/Core.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define unreachable() assert(!"unreachable")
#define not_implemented() assert(!"not implemented");
#define safe_ptr(ptr) ((typeof(ptr)) safe_ptr_impl(ptr))
#define arrlen(arr) (sizeof(arr) / sizeof(arr[0]))

void *safe_ptr_impl(void *ptr)
{
	assert(ptr);
	return ptr;
}

typedef uint8_t u8;
typedef int8_t i8;
typedef uint16_t u16;
typedef int16_t i16;
typedef uint32_t u32;
typedef int32_t i32;
typedef uint64_t u64;
typedef int64_t i64;

__attribute__((format(printf, 1, 2)))
char *aprintf(char *format, ...)
{
	va_list ap;

	va_start(ap, format);
	int size = vsnprintf(NULL, 0, format, ap) + 1;
	va_end(ap);

	char *buf = safe_ptr(malloc(size));

	va_start(ap, format);
	vsnprintf(buf, size, format, ap);
	va_end(ap);

	return buf;
}

typedef struct {
	char *file;
	u32 line;
	u32 col;
} Loc;

Loc Loc_AdvanceCol(Loc l, u32 a)
{
	l.col += a;
	return l;
}

__attribute__((noreturn))
__attribute__((format(printf, 1, 2)))
void panic(char *format, ...)
{
	va_list ap;
	va_start(ap, format);

	fprintf(stderr, "panic: ");
	vfprintf(stderr, format, ap);
	fputc('\n', stderr);

	va_end(ap);
	exit(1);
}

__attribute__((noreturn))
__attribute__((format(printf, 2, 3)))
void fatal(Loc loc, char *format, ...)
{
	va_list ap;
	va_start(ap, format);

	fprintf(stderr, "%s:%d:%d: fatal: ", loc.file, loc.line, loc.col);
	vfprintf(stderr, format, ap);
	fputc('\n', stderr);

	va_end(ap);
	exit(1);
}

bool isident(int c)
{
	return isalnum(c) || c == '_';
}

typedef struct {
	char *chars;
	u32 count;
	u32 cap;
} StringBuilder;

void StringBuilder_Append(StringBuilder *b, char c)
{
	if(b->count == b->cap) {
		b->cap += 64;
		b->chars = safe_ptr(realloc(b->chars, b->cap));
	}

	b->chars[b->count++] = c;
}

void StringBuilder_AppendString(StringBuilder *b, char *s)
{
	for(u32 i = 0; s[i]; i++)
		StringBuilder_Append(b, s[i]);
}

char *StringBuilder_Finalize(StringBuilder *b)
{
	if(b->count == b->cap) {
		b->cap++;
		b->chars = safe_ptr(realloc(b->chars, b->cap));
	}

	b->chars[b->count] = 0;

	char *str = b->chars;
	*b = (StringBuilder) {0};
	return str;
}

typedef enum {
	TOKEN_LPAREN,
	TOKEN_RPAREN,
	TOKEN_LBRACE,
	TOKEN_RBRACE,
	TOKEN_LBRACKET,
	TOKEN_RBRACKET,
	TOKEN_COLON,
	TOKEN_SEMICOLON,
	TOKEN_COMMA,
	TOKEN_ASTERISK,
	TOKEN_SLASH,
	TOKEN_PERCENT,
	TOKEN_PLUS,
	TOKEN_MINUS,
	TOKEN_DOUBLE_EQUAL,
	TOKEN_EQUAL,
	TOKEN_NOT_EQUAL,
	TOKEN_NOT,
	TOKEN_GREATER_EQUAL,
	TOKEN_GREATER,
	TOKEN_LESSER_EQUAL,
	TOKEN_LESSER,

	TOKEN_KW_FN,
	TOKEN_KW_RETURN,
	TOKEN_KW_EXTERN,
	TOKEN_KW_CONST,
	TOKEN_KW_VAR,
	TOKEN_KW_TRUE,
	TOKEN_KW_FALSE,
	TOKEN_KW_IF,
	TOKEN_KW_ELSE,
	TOKEN_KW_WHILE,

	TOKEN_INTEGER,
	TOKEN_IDENTIFIER,
	TOKEN_STRING,

	TOKEN_EOF,
	COUNT_TOKEN
} TokenKind;

char *TokenKind_ToString(TokenKind k, bool has_article)
{
	static_assert(COUNT_TOKEN == 36);
	switch(k) {
	case TOKEN_LPAREN:
		return "`(`";
	case TOKEN_RPAREN:
		return "`)`";
	case TOKEN_LBRACE:
		return "`{`";
	case TOKEN_RBRACE:
		return "`}`";
	case TOKEN_LBRACKET:
		return "`[`";
	case TOKEN_RBRACKET:
		return "`]`";
	case TOKEN_COLON:
		return "`:`";
	case TOKEN_SEMICOLON:
		return "`;`";
	case TOKEN_COMMA:
		return "`,`";
	case TOKEN_ASTERISK:
		return "`*`";
	case TOKEN_SLASH:
		return "`/`";
	case TOKEN_PERCENT:
		return "`%`";
	case TOKEN_PLUS:
		return "`+`";
	case TOKEN_MINUS:
		return "`-`";
	case TOKEN_DOUBLE_EQUAL:
		return "`==`";
	case TOKEN_EQUAL:
		return "`=`";
	case TOKEN_NOT_EQUAL:
		return "`~=`";
	case TOKEN_NOT:
		return "`~`";
	case TOKEN_GREATER_EQUAL:
		return "`>=`";
	case TOKEN_GREATER:
		return "`>`";
	case TOKEN_LESSER_EQUAL:
		return "`<=`";
	case TOKEN_LESSER:
		return "`<`";
	case TOKEN_KW_FN:
		return "`fn`";
	case TOKEN_KW_RETURN:
		return "`return`";
	case TOKEN_KW_EXTERN:
		return "`extern`";
	case TOKEN_KW_CONST:
		return "`const`";
	case TOKEN_KW_VAR:
		return "`var`";
	case TOKEN_KW_TRUE:
		return "`true`";
	case TOKEN_KW_FALSE:
		return "`false`";
	case TOKEN_KW_IF:
		return "`if`";
	case TOKEN_KW_ELSE:
		return "`else`";
	case TOKEN_KW_WHILE:
		return "`while`";
	case TOKEN_INTEGER:
		return has_article ? "an integer" : "integer";
	case TOKEN_IDENTIFIER:
		return has_article ? "an identifier" : "identifier";
	case TOKEN_STRING:
		return has_article ? "a string" : "string";
	case TOKEN_EOF:
		return has_article ? "the end of file" : "end of file";
	default:
		unreachable();
	}
}

typedef struct {
	TokenKind kind;
	Loc loc;
	union {
		u64 token_integer;
		char *token_identifier;
		char *token_string;
	} as;
} Token;

typedef struct {
	FILE *input;
	Loc loc;

	bool has_token;
	Token token;
} Lexer;

Lexer Lexer_Init(char *input_file)
{
	FILE *input = fopen(input_file, "r");
	if(!input)
		panic("cannot open `%s`: %s", input_file, strerror(errno));

	return (Lexer) {
		.input = input,
		.loc = {
			.file = input_file,
			.line = 1,
			.col = 1
		},
		.has_token = false
	};
}

void Lexer_Close(Lexer *l)
{
	fclose(l->input);
}

int Lexer_ParseChar(Lexer *l)
{
	int c = fgetc(l->input);
	if(!isprint(c))
		fatal(l->loc, "literals can only contain printable characters; consider using escape codes");
	l->loc.col++;

	if(c == '\\') {
		c = fgetc(l->input);
		switch(c) {
		case 'n':
			l->loc.col++;
			return '\n';
		default:
			fatal(l->loc, "invalid escape code");
		}
	}

	return c;
}

void Lexer_Next(Lexer *l)
{
	static_assert(COUNT_TOKEN == 36);

	assert(!l->has_token);
	l->has_token = true;

	int c;

	while(true) {
		c = fgetc(l->input);
		if(isspace(c)) {
			if(c == '\n') {
				l->loc.line++;
				l->loc.col = 1;
			} else {
				l->loc.col++;
			}
			continue;
		}

		if(c == '#') {
			while(c != EOF) {
				c = fgetc(l->input);
				if(c == '\n') {
					l->loc.line++;
					l->loc.col = 1;
					break;
				}
			}
			continue;
		}

		break;
	}

	l->token.loc = l->loc;

	if(c == '\'') {
		l->token.kind = TOKEN_INTEGER;

		l->loc.col++;
		if((c = fgetc(l->input)) == '\'')
			fatal(l->token.loc, "empty character literal");
		ungetc(c, l->input);

		l->token.as.token_integer = Lexer_ParseChar(l);

		if(fgetc(l->input) != '\'')
			fatal(l->token.loc, "unclosed character literal");
		l->loc.col++;
		return;
	}

	if(c == '"') {
		StringBuilder b = {0};
		l->token.kind = TOKEN_STRING;

		l->loc.col++;
		while((c = fgetc(l->input)) != '"') {
			if(c == EOF)
				fatal(l->token.loc, "unclosed string literal");

			ungetc(c, l->input);
			StringBuilder_Append(&b, Lexer_ParseChar(l));
		}
		l->loc.col++;

		l->token.as.token_string = StringBuilder_Finalize(&b);
		return;
	}

	if(isident(c)) {
		StringBuilder b = {0};
		do {
			StringBuilder_Append(&b, c);
		} while(isident(c = fgetc(l->input)));
		ungetc(c, l->input);
		l->loc.col += b.count;

		char *word = StringBuilder_Finalize(&b);

		if(!strcmp(word, "fn")) {
			l->token.kind = TOKEN_KW_FN;
		} else if(!strcmp(word, "return")) {
			l->token.kind = TOKEN_KW_RETURN;
		} else if(!strcmp(word, "extern")) {
			l->token.kind = TOKEN_KW_EXTERN;
		} else if(!strcmp(word, "const")) {
			l->token.kind = TOKEN_KW_CONST;
		} else if(!strcmp(word, "var")) {
			l->token.kind = TOKEN_KW_VAR;
		} else if(!strcmp(word, "true")) {
			l->token.kind = TOKEN_KW_TRUE;
		} else if(!strcmp(word, "false")) {
			l->token.kind = TOKEN_KW_FALSE;
		} else if(!strcmp(word, "if")) {
			l->token.kind = TOKEN_KW_IF;
		} else if(!strcmp(word, "else")) {
			l->token.kind = TOKEN_KW_ELSE;
		} else if(!strcmp(word, "while")) {
			l->token.kind = TOKEN_KW_WHILE;
		} else if(isdigit(word[0])) {
			u64 num = 0;
			for(u32 i = 0; word[i]; i++) {
				if(!isdigit(word[i]))
					fatal(Loc_AdvanceCol(l->token.loc, i), "invalid character in an integer literal");
				num = num * 10 + word[i] - '0';
			}
			l->token.kind = TOKEN_INTEGER;
			l->token.as.token_integer = num;
		} else {
			l->token.kind = TOKEN_IDENTIFIER;
			l->token.as.token_identifier = word;
		}

		return;
	}

	l->loc.col++;
	switch(c) {
	case '(':
		l->token.kind = TOKEN_LPAREN;
		break;
	case ')':
		l->token.kind = TOKEN_RPAREN;
		break;
	case '{':
		l->token.kind = TOKEN_LBRACE;
		break;
	case '}':
		l->token.kind = TOKEN_RBRACE;
		break;
	case '[':
		l->token.kind = TOKEN_LBRACKET;
		break;
	case ']':
		l->token.kind = TOKEN_RBRACKET;
		break;
	case ':':
		l->token.kind = TOKEN_COLON;
		break;
	case ';':
		l->token.kind = TOKEN_SEMICOLON;
		break;
	case ',':
		l->token.kind = TOKEN_COMMA;
		break;
	case '*':
		l->token.kind = TOKEN_ASTERISK;
		break;
	case '/':
		l->token.kind = TOKEN_SLASH;
		break;
	case '%':
		l->token.kind = TOKEN_PERCENT;
		break;
	case '+':
		l->token.kind = TOKEN_PLUS;
		break;
	case '-':
		l->token.kind = TOKEN_MINUS;
		break;
	case '=':
		if((c = fgetc(l->input)) == '=') {
			l->loc.col++;
			l->token.kind = TOKEN_DOUBLE_EQUAL;
		} else {
			ungetc(c, l->input);
			l->token.kind = TOKEN_EQUAL;
		}
		break;
	case '~':
		if((c = fgetc(l->input)) == '=') {
			l->loc.col++;
			l->token.kind = TOKEN_NOT_EQUAL;
		} else {
			ungetc(c, l->input);
			l->token.kind = TOKEN_NOT;
		}
		break;
	case '>':
		if((c = fgetc(l->input)) == '=') {
			l->loc.col++;
			l->token.kind = TOKEN_GREATER_EQUAL;
		} else {
			ungetc(c, l->input);
			l->token.kind = TOKEN_GREATER;
		}
		break;
	case '<':
		if((c = fgetc(l->input)) == '=') {
			l->loc.col++;
			l->token.kind = TOKEN_LESSER_EQUAL;
		} else {
			ungetc(c, l->input);
			l->token.kind = TOKEN_LESSER;
		}
		break;
	case EOF:
		l->token.kind = TOKEN_EOF;
		break;
	default:
		fatal(l->token.loc, "invalid character");
	}
}

TokenKind Lexer_Peek(Lexer *l)
{
	if(!l->has_token)
		Lexer_Next(l);
	return l->token.kind;
}

Token Lexer_Pop(Lexer *l)
{
	assert(l->has_token);
	l->has_token = false;
	return l->token;
}

__attribute__((noreturn))
void Lexer_Unexpected(Lexer *l, char *expected)
{
	assert(l->has_token);
	fatal(l->token.loc, "unexpected %s; expected %s", TokenKind_ToString(l->token.kind, false), expected);
}

Token Lexer_Expect(Lexer *l, TokenKind expected)
{
	if(Lexer_Peek(l) != expected)
		Lexer_Unexpected(l, TokenKind_ToString(expected, true));
	return Lexer_Pop(l);
}

typedef struct Ptr Ptr;
typedef struct Slice Slice;
typedef struct Fn Fn;
typedef struct PtrType PtrType;
typedef struct SliceType SliceType;
typedef struct FnType FnType;
typedef struct Call Call;
typedef struct BinOp BinOp;
typedef struct Type Type;
typedef struct Expr Expr;
typedef struct Stmt Stmt;
int Expr_Parse(Lexer *l, Expr *out);
Stmt *Stmt_ParseBody(Lexer *l, u32 *out_body_len);

typedef enum {
	TYPE_VOID,
	TYPE_BOOL,
	TYPE_INT,
	TYPE_PTR,
	TYPE_SLICE,
	TYPE_FN,
	TYPE_TYPE,
	COUNT_TYPE
} TypeKind;

struct Type {
	TypeKind kind;
	union {
		struct {
			bool sign;
			u8 bits;
		} type_int;
		Ptr *type_ptr;
		Slice *type_slice;
		Fn *type_fn;
	} as;
};

struct Ptr {
	Type pointee;
};

struct Slice {
	Type item;
};

struct Fn {
	Type ret_type;
	Type *args;
	u32 args_count;
};

int Type_Compare(Type *a, Type *b)
{
	if(a->kind != b->kind)
		return -1;

	static_assert(COUNT_TYPE == 7);
	switch(a->kind) {
	case TYPE_VOID:
	case TYPE_BOOL:
	case TYPE_TYPE:
		return 0;
	case TYPE_INT:
		return a->as.type_int.sign != b->as.type_int.sign || a->as.type_int.bits != b->as.type_int.bits;
	case TYPE_PTR:
		return Type_Compare(&a->as.type_ptr->pointee, &b->as.type_ptr->pointee);
	case TYPE_SLICE:
		return Type_Compare(&a->as.type_slice->item, &b->as.type_slice->item);
	case TYPE_FN:
		if(a->as.type_fn->args_count != b->as.type_fn->args_count)
			return 1;

		for(u32 i = 0; i < a->as.type_fn->args_count; i++) {
			if(Type_Compare(&a->as.type_fn->args[i], &b->as.type_fn->args[i]))
				return 1;
		}

		return Type_Compare(&a->as.type_fn->ret_type, &b->as.type_fn->ret_type);
	default:
		unreachable();
	}
}

char *Type_ToString(Type *t)
{
	static_assert(COUNT_TYPE == 7);
	switch(t->kind) {
	case TYPE_VOID:
		return "void";
	case TYPE_BOOL:
		return "bool";
	case TYPE_INT:
		return aprintf("%c%d", t->as.type_int.sign ? 'i' : 'u', t->as.type_int.bits);
	case TYPE_PTR:
		return aprintf("*%s", Type_ToString(&t->as.type_ptr->pointee));
	case TYPE_SLICE:
		return aprintf("[]%s", Type_ToString(&t->as.type_slice->item));
	case TYPE_FN: {
		StringBuilder b = {0};
		StringBuilder_AppendString(&b, "fn (");
		for(u32 i = 0; i < t->as.type_fn->args_count; i++) {
			if(i)
				StringBuilder_AppendString(&b, ", ");
			StringBuilder_AppendString(&b, Type_ToString(&t->as.type_fn->args[i]));
		}
		StringBuilder_AppendString(&b, ") ");
		StringBuilder_AppendString(&b, Type_ToString(&t->as.type_fn->ret_type));
		return StringBuilder_Finalize(&b); }
	case TYPE_TYPE:
		return "type";
	default:
		unreachable();
	}
}

void Type_ValidateCast(Loc loc, Type *from, Type *to)
{
	static_assert(COUNT_TYPE == 7);
	switch(to->kind) {
	case TYPE_VOID:
	case TYPE_BOOL:
	case TYPE_PTR:
	case TYPE_SLICE:
	case TYPE_FN:
	case TYPE_TYPE:
		fatal(loc, "cannot cast to `%s`", Type_ToString(to));
	case TYPE_INT:
		if(from->kind == TYPE_INT)
			break;
		fatal(loc, "cannot cast from a non-integer type `%s` to an integer", Type_ToString(from));
	default:
		unreachable();
	}
}

typedef enum {
	EXPR_INT_LIT,
	EXPR_BOOL_LIT,
	EXPR_STR_LIT,
	EXPR_IDENTIFIER,
	EXPR_PTR_TYPE,
	EXPR_SLICE_TYPE,
	EXPR_FN_TYPE,
	EXPR_CALL,
	EXPR_BIN_OP,
	COUNT_EXPR
} ExprKind;

struct Expr {
	ExprKind kind;
	Loc loc;
	union {
		u64 expr_int_lit;
		bool expr_bool_lit;
		char *expr_str_lit;
		char *expr_identifier;
		PtrType *expr_ptr_type;
		SliceType *expr_slice_type;
		FnType *expr_fn_type;
		Call *expr_call;
		BinOp *expr_bin_op;
	} as;
	Type type; // after validation
	void *comptime_val; // after validation; might be NULL
	bool cast; // after validation
	Type cast_to; // after validation
	bool is_lvalue; // after validation
	bool constant; // after validation
};

struct PtrType {
	Expr pointee;
};

struct SliceType {
	Expr item;
};

struct FnType {
	Expr ret_type;
	Expr *args;
	u32 args_count;
};

struct Call {
	Expr fn;
	Expr *args;
	u32 args_count;
};

typedef enum {
	BIN_OP_CAST,
	BIN_OP_MUL,
	BIN_OP_DIV,
	BIN_OP_MOD,
	BIN_OP_ADD,
	BIN_OP_SUB,
	BIN_OP_EQ,
	BIN_OP_NE,
	BIN_OP_GE,
	BIN_OP_GT,
	BIN_OP_LE,
	BIN_OP_LT,
	COUNT_BIN_OP
} BinOpKind;

struct BinOp {
	BinOpKind kind;
	Expr lhs;
	Expr rhs;
};

// all types are primary expressions
int Expr_ParsePrimary(Lexer *l, Expr *out)
{
	Token tok;
	switch(Lexer_Peek(l)) {
	case TOKEN_LBRACKET:
		out->kind = EXPR_SLICE_TYPE;
		tok = Lexer_Pop(l);
		Lexer_Expect(l, TOKEN_RBRACKET);
		{
			SliceType *s = safe_ptr(malloc(sizeof(*s)));
			if(Expr_ParsePrimary(l, &s->item))
				Lexer_Unexpected(l, "the type of slice's element");
			out->loc = tok.loc;
			out->as.expr_slice_type = s;
		}
		break;
	case TOKEN_ASTERISK:
		out->kind = EXPR_PTR_TYPE;
		tok = Lexer_Pop(l);
		{
			PtrType *s = safe_ptr(malloc(sizeof(*s)));
			if(Expr_ParsePrimary(l, &s->pointee))
				Lexer_Unexpected(l, "the pointee type");
			out->loc = tok.loc;
			out->as.expr_ptr_type = s;
		}
		break;
	case TOKEN_KW_FN:
		tok = Lexer_Pop(l);
		out->kind = EXPR_FN_TYPE;
		out->loc = tok.loc;
		Lexer_Expect(l, TOKEN_LPAREN);
		out->as.expr_fn_type = safe_ptr(malloc(sizeof(*out->as.expr_fn_type)));

		out->as.expr_fn_type->args = NULL;
		out->as.expr_fn_type->args_count = 0;
		u32 args_cap = 0;
		Expr arg;
		while(!Expr_ParsePrimary(l, &arg)) {
			if(out->as.expr_fn_type->args_count == args_cap) {
				args_cap += 8;
				out->as.expr_fn_type->args = safe_ptr(realloc(out->as.expr_fn_type->args, sizeof(*out->as.expr_fn_type->args) * args_cap));
			}

			out->as.expr_fn_type->args[out->as.expr_fn_type->args_count++] = arg;
			if(Lexer_Peek(l) != TOKEN_COMMA)
				break;
			Lexer_Pop(l);
		}

		Lexer_Expect(l, TOKEN_RPAREN);
		if(Expr_ParsePrimary(l, &out->as.expr_fn_type->ret_type))
			Lexer_Unexpected(l, "the return type");
		break;
	case TOKEN_KW_TRUE:
	case TOKEN_KW_FALSE:
		tok = Lexer_Pop(l);
		out->kind = EXPR_BOOL_LIT;
		out->loc = tok.loc;
		out->as.expr_bool_lit = tok.kind == TOKEN_KW_TRUE;
		break;
	case TOKEN_INTEGER:
		tok = Lexer_Pop(l);
		out->kind = EXPR_INT_LIT;
		out->loc = tok.loc;
		out->as.expr_int_lit = tok.as.token_integer;
		break;
	case TOKEN_STRING:
		tok = Lexer_Pop(l);
		out->kind = EXPR_STR_LIT;
		out->loc = tok.loc;
		out->as.expr_str_lit = tok.as.token_string;
		break;
	case TOKEN_IDENTIFIER:
		tok = Lexer_Pop(l);
		out->kind = EXPR_IDENTIFIER;
		out->loc = tok.loc;
		out->as.expr_identifier = tok.as.token_identifier;
		break;
		return 0;
	default:
		return -1;
	}

	while(Lexer_Peek(l) == TOKEN_LPAREN) {
		Lexer_Pop(l);

		Call *call = safe_ptr(malloc(sizeof(*call)));
		call->fn = *out;
		call->args = NULL;
		call->args_count = 0;
		u32 args_cap = 0;

		Expr arg;
		while(!Expr_Parse(l, &arg)) {
			if(call->args_count == args_cap) {
				args_cap += 8;
				call->args = realloc(call->args, args_cap * sizeof(*call->args));
			}

			call->args[call->args_count++] = arg;
			if(Lexer_Peek(l) != TOKEN_COMMA)
				break;
			Lexer_Pop(l);
		}

		Lexer_Expect(l, TOKEN_RPAREN);

		out->kind = EXPR_CALL;
		out->as.expr_call = call;
	}

	return 0;
}

bool BinOpKind_ValidToken(TokenKind t)
{
	static_assert(COUNT_BIN_OP == 12);
	switch(t) {
	case TOKEN_COLON:
	case TOKEN_ASTERISK:
	case TOKEN_SLASH:
	case TOKEN_PERCENT:
	case TOKEN_PLUS:
	case TOKEN_MINUS:
	case TOKEN_DOUBLE_EQUAL:
	case TOKEN_NOT_EQUAL:
	case TOKEN_GREATER_EQUAL:
	case TOKEN_GREATER:
	case TOKEN_LESSER_EQUAL:
	case TOKEN_LESSER:
		return true;
	default:
		return false;
	}
}

u32 BinOpKind_GetPrec(BinOpKind b)
{
	u32 prec = 0;

	static_assert(COUNT_BIN_OP == 12);
	switch(b) {
	case BIN_OP_CAST:
		prec++; __attribute__((fallthrough));
	case BIN_OP_MUL:
	case BIN_OP_DIV:
	case BIN_OP_MOD:
		prec++; __attribute__((fallthrough));
	case BIN_OP_ADD:
	case BIN_OP_SUB:
		prec++;
	case BIN_OP_EQ:
	case BIN_OP_NE:
	case BIN_OP_GE:
	case BIN_OP_GT:
	case BIN_OP_LE:
	case BIN_OP_LT:
		break;
	default:
		unreachable();
	}

	return prec;
}

BinOpKind BinOpKind_FromToken(TokenKind t)
{
	static_assert(COUNT_BIN_OP == 12);
	switch(t) {
	case TOKEN_COLON:
		return BIN_OP_CAST;
	case TOKEN_ASTERISK:
		return BIN_OP_MUL;
	case TOKEN_SLASH:
		return BIN_OP_DIV;
	case TOKEN_PERCENT:
		return BIN_OP_MOD;
	case TOKEN_PLUS:
		return BIN_OP_ADD;
	case TOKEN_MINUS:
		return BIN_OP_SUB;
	case TOKEN_DOUBLE_EQUAL:
		return BIN_OP_EQ;
	case TOKEN_NOT_EQUAL:
		return BIN_OP_NE;
	case TOKEN_GREATER_EQUAL:
		return BIN_OP_GE;
	case TOKEN_GREATER:
		return BIN_OP_GT;
	case TOKEN_LESSER_EQUAL:
		return BIN_OP_LE;
	case TOKEN_LESSER:
		return BIN_OP_LT;
	default:
		unreachable();
	}
}

// Shamelessly stolen from https://en.wikipedia.org/wiki/Operator-precedence_parser#Pseudocode
// out must contain the lhs
void Expr_BinOp_Parse(Lexer *l, Expr *out, u32 prec)
{
	TokenKind op_tok = Lexer_Peek(l);
	BinOpKind op;
	while(BinOpKind_ValidToken(op_tok)
		? BinOpKind_GetPrec(op = BinOpKind_FromToken(op_tok)) >= prec
		: false) {
		Lexer_Pop(l);

		Expr rhs;
		if(Expr_ParsePrimary(l, &rhs))
			fatal(l->token.loc, "expected the right hand side of the operation");

		op_tok = Lexer_Peek(l);
		while(BinOpKind_ValidToken(op_tok)
			? BinOpKind_GetPrec(BinOpKind_FromToken(op_tok)) > BinOpKind_GetPrec(op)
			: false) {
			Expr_BinOp_Parse(l, &rhs, BinOpKind_GetPrec(op) + (BinOpKind_GetPrec(BinOpKind_FromToken(op_tok)) > BinOpKind_GetPrec(op)));
			op_tok = Lexer_Peek(l);
		}

		BinOp *bin_op = safe_ptr(malloc(sizeof(*bin_op)));
		bin_op->kind = op;
		bin_op->lhs = *out;
		bin_op->rhs = rhs;

		out->kind = EXPR_BIN_OP;
		out->as.expr_bin_op = bin_op;
	}
}

int Expr_Parse(Lexer *l, Expr *out)
{
	static_assert(COUNT_EXPR == 9);
	if(Expr_ParsePrimary(l, out))
		return -1;

	Expr_BinOp_Parse(l, out, 0);
	return 0;
}

typedef enum {
	STMT_FN_DECL,
	STMT_RETURN,
	STMT_EXPR,
	STMT_VAR,
	STMT_ASSIGN,
	STMT_IF,
	STMT_WHILE,
	COUNT_STMT
} StmtKind;

typedef struct {
	Loc loc;
	char *name;
	Expr type;
} FnArgDecl;

struct Stmt {
	StmtKind kind;
	Loc loc;
	union {
		struct {
			char *name;
			FnArgDecl *args;
			u32 args_count;
			bool has_ret_type;
			Expr ret_type;
			bool external;

			Stmt *body;
			u32 body_len;
			bool implicit_return; // after validation
			Type fn_type; // after validation
		} stmt_fn_decl;
		struct {
			bool has_value;
			Expr value;
		} stmt_return;
		Expr stmt_expr;
		struct {
			char *name;
			Expr val;
			bool constant;
		} stmt_var;
		struct {
			Expr lhs;
			Expr rhs;
		} stmt_assign;
		struct {
			Expr cond;
			Stmt *body;
			u32 body_len;
			Stmt *else_body;
			u32 else_len;
		} stmt_if;
		struct {
			Expr cond;
			Stmt *body;
			u32 body_len;
		} stmt_while;
	} as;
};

void Stmt_FnDecl_Parse(Lexer *l, Stmt *out, bool external)
{
	out->kind = STMT_FN_DECL;
	out->loc = Lexer_Pop(l).loc;

	out->as.stmt_fn_decl.name = Lexer_Expect(l, TOKEN_IDENTIFIER).as.token_identifier;

	Lexer_Expect(l, TOKEN_LPAREN);

	out->as.stmt_fn_decl.args = NULL;
	out->as.stmt_fn_decl.args_count = 0;
	u32 args_cap = 0;

	while(Lexer_Peek(l) == TOKEN_IDENTIFIER) {
		Token tok = Lexer_Pop(l);
		FnArgDecl arg;
		arg.loc = tok.loc;
		arg.name = tok.as.token_identifier;

		Lexer_Expect(l, TOKEN_COLON);

		if(Expr_Parse(l, &arg.type))
			Lexer_Unexpected(l, "the type of the argument");

		if(out->as.stmt_fn_decl.args_count == args_cap) {
			args_cap += 8;
			out->as.stmt_fn_decl.args = safe_ptr(realloc(out->as.stmt_fn_decl.args, args_cap * sizeof(*out->as.stmt_fn_decl.args)));
		}

		out->as.stmt_fn_decl.args[out->as.stmt_fn_decl.args_count++] = arg;

		if(Lexer_Peek(l) != TOKEN_COMMA)
			break;
		Lexer_Pop(l);
	}

	Lexer_Expect(l, TOKEN_RPAREN);

	out->as.stmt_fn_decl.has_ret_type = Expr_Parse(l, &out->as.stmt_fn_decl.ret_type) == 0;

	if(external)
		Lexer_Expect(l, TOKEN_SEMICOLON);
	else
		out->as.stmt_fn_decl.body = Stmt_ParseBody(l, &out->as.stmt_fn_decl.body_len);

	out->as.stmt_fn_decl.external = external;
}

void Stmt_Return_Parse(Lexer *l, Stmt *out)
{
	out->kind = STMT_RETURN;
	out->loc = Lexer_Pop(l).loc;

	out->as.stmt_return.has_value = Expr_Parse(l, &out->as.stmt_return.value) == 0;

	Lexer_Expect(l, TOKEN_SEMICOLON);
}

int Stmt_Expr_Parse(Lexer *l, Stmt *out)
{
	Expr expr;
	if(Expr_Parse(l, &expr))
		return -1;
	out->loc = expr.loc;

	if(Lexer_Peek(l) == TOKEN_EQUAL) {
		Lexer_Pop(l);
		out->kind = STMT_ASSIGN;
		out->as.stmt_assign.lhs = expr;

		if(Expr_Parse(l, &out->as.stmt_assign.rhs))
			Lexer_Unexpected(l, "the right hand side of the assignment");
	} else {
		out->kind = STMT_EXPR;
		out->as.stmt_expr = expr;
	}

	Lexer_Expect(l, TOKEN_SEMICOLON);
	return 0;
}

void Stmt_Extern_Parse(Lexer *l, Stmt *out)
{
	Loc loc = Lexer_Pop(l).loc;

	switch(Lexer_Peek(l)) {
	case TOKEN_KW_FN:
		Stmt_FnDecl_Parse(l, out, true);
		break;
	default:
		Lexer_Unexpected(l, "a declaration");
	}

	out->loc = loc;
}

void Stmt_Var_Parse(Lexer *l, Stmt *out, bool constant)
{
	out->loc = Lexer_Pop(l).loc;
	out->kind = STMT_VAR;
	out->as.stmt_var.name = Lexer_Expect(l, TOKEN_IDENTIFIER).as.token_identifier;
	out->as.stmt_var.constant = constant;

	Lexer_Expect(l, TOKEN_EQUAL);

	if(Expr_Parse(l, &out->as.stmt_var.val))
		Lexer_Unexpected(l, "an expression");

	Lexer_Expect(l, TOKEN_SEMICOLON);
}

void Stmt_If_Parse(Lexer *l, Stmt *out)
{
	out->loc = Lexer_Pop(l).loc;
	out->kind = STMT_IF;

	Lexer_Expect(l, TOKEN_LPAREN);
	if(Expr_Parse(l, &out->as.stmt_if.cond))
		Lexer_Unexpected(l, "a condition");
	Lexer_Expect(l, TOKEN_RPAREN);

	out->as.stmt_if.body = Stmt_ParseBody(l, &out->as.stmt_if.body_len);

	if(Lexer_Peek(l) == TOKEN_KW_ELSE) {
		Lexer_Pop(l);
		out->as.stmt_if.else_body = Stmt_ParseBody(l, &out->as.stmt_if.else_len);
	} else {
		out->as.stmt_if.else_len = 0;
	}
}

void Stmt_While_Parse(Lexer *l, Stmt *out)
{
	out->loc = Lexer_Pop(l).loc;
	out->kind = STMT_WHILE;

	Lexer_Expect(l, TOKEN_LPAREN);
	if(Expr_Parse(l, &out->as.stmt_while.cond))
		Lexer_Unexpected(l, "a condition");
	Lexer_Expect(l, TOKEN_RPAREN);

	out->as.stmt_while.body = Stmt_ParseBody(l, &out->as.stmt_while.body_len);
}

int Stmt_Parse(Lexer *l, Stmt *out)
{
	static_assert(COUNT_STMT == 7);
	switch(Lexer_Peek(l)) {
	case TOKEN_KW_FN:
		Stmt_FnDecl_Parse(l, out, false);
		return 0;
	case TOKEN_KW_RETURN:
		Stmt_Return_Parse(l, out);
		return 0;
	case TOKEN_KW_EXTERN:
		Stmt_Extern_Parse(l, out);
		return 0;
	case TOKEN_KW_CONST:
		Stmt_Var_Parse(l, out, true);
		return 0;
	case TOKEN_KW_VAR:
		Stmt_Var_Parse(l, out, false);
		return 0;
	case TOKEN_KW_IF:
		Stmt_If_Parse(l, out);
		return 0;
	case TOKEN_KW_WHILE:
		Stmt_While_Parse(l, out);
		return 0;
	default:
		return Stmt_Expr_Parse(l, out);
	}
}

// parse as many statements as possible
Stmt *Stmt_ParseMany(Lexer *l, u32 *out_len)
{
	Stmt *body = NULL;
	*out_len = 0;
	u32 cap = 0;

	Stmt s;
	while(!Stmt_Parse(l, &s)) {
		if(*out_len == cap) {
			cap += 64;
			body = safe_ptr(realloc(body, cap * sizeof(*body)));
		}

		body[(*out_len)++] = s;
	}

	return body;
}

// parse either a single statement or a group of statements inside braces
Stmt *Stmt_ParseBody(Lexer *l, u32 *out_body_len)
{
	if(Lexer_Peek(l) != TOKEN_LBRACE) {
		*out_body_len = 1;
		Stmt *s = safe_ptr(malloc(sizeof(*s)));
		if(Stmt_Parse(l, s))
			Lexer_Unexpected(l, "a body");
		return s;
	}

	Lexer_Pop(l);
	Stmt *ret = Stmt_ParseMany(l, out_body_len);
	Lexer_Expect(l, TOKEN_RBRACE);
	return ret;
}

typedef struct {
	char *name;
	Type type; // only validation
	bool is_lvalue; // only validation
	bool constant; // only validation; must be true if is_lvalue == false
	union {
		void *validation; // might be NULL
		LLVMValueRef codegen;
	} value;
} ScopeItem;

typedef struct {
	ScopeItem *items;
	u32 count;
	u32 cap;
} Scope;

void Scope_Define(Scope *s, Loc loc, ScopeItem item)
{
	for(u32 i = 0; i < s->count; i++) {
		if(!strcmp(s->items[i].name, item.name))
			fatal(loc, "redefinition of `%s`", item.name);
	}

	if(s->count == s->cap) {
		s->cap += 64;
		s->items = safe_ptr(realloc(s->items, s->cap * sizeof(*s->items)));
	}

	s->items[s->count++] = item;
}

void Scope_DefineInValidation(Scope *s, Loc loc, char *name, Type type, void *value, bool is_lvalue, bool constant)
{
	Scope_Define(s, loc, ((ScopeItem) { .name = name, .type = type, .is_lvalue = is_lvalue, .constant = constant, .value.validation = value }));
}

void Scope_DefineInCodegen(Scope *s, Loc loc, char *name, LLVMValueRef value)
{
	Scope_Define(s, loc, ((ScopeItem) { .name = name, .value.codegen = value }));
}

ScopeItem Scope_Find(Scope *s, Loc loc, char *name)
{
	for(u32 i = 0; i < s->count; i++) {
		if(!strcmp(s->items[i].name, name))
			return s->items[i];
	}

	fatal(loc, "undefined identifier `%s`", name);
}

typedef struct {
	bool in_func;
	Type ret_type;
	bool returned;
	Scope scope;
} ValidationCtx;

void Stmt_ValidateMany(Stmt *s, u32 len, ValidationCtx *ctx);

Slice type_string_slice = {
	.item = {
		.kind = TYPE_INT,
		.as.type_int = {
			.sign = true,
			.bits = 8
		}
	}
};

Type type_string = {
	.kind = TYPE_SLICE,
	.as.type_slice = &type_string_slice
};

void Expr_Validate(Expr *e, ValidationCtx *ctx)
{
	e->cast = false;
	e->is_lvalue = false;
	e->constant = false;
	static_assert(COUNT_EXPR == 9);
	switch(e->kind) {
	case EXPR_INT_LIT:
		e->type.kind = TYPE_INT;
		e->type.as.type_int.sign = false;
		e->type.as.type_int.bits = 64;
		break;
	case EXPR_BOOL_LIT:
		e->type.kind = TYPE_BOOL;
		break;
	case EXPR_STR_LIT:
		e->type = type_string;
		break;
	case EXPR_IDENTIFIER: {
		ScopeItem item = Scope_Find(&ctx->scope, e->loc, e->as.expr_identifier);
		e->type = item.type;
		e->comptime_val = item.value.validation;
		e->is_lvalue = item.is_lvalue;
		e->constant = item.constant;
		break; }
	case EXPR_PTR_TYPE:
		Expr_Validate(&e->as.expr_ptr_type->pointee, ctx);
		if(e->as.expr_ptr_type->pointee.type.kind != TYPE_TYPE)
			fatal(e->loc, "the pointee type is not a type");
		e->type.kind = TYPE_TYPE;
		e->comptime_val = safe_ptr(malloc(sizeof(Type)));
		*(Type*)e->comptime_val = (Type) {
			.kind = TYPE_PTR,
			.as.type_ptr = safe_ptr(malloc(sizeof(Ptr)))
		};
		((Type*)e->comptime_val)->as.type_ptr->pointee = *(Type*)e->as.expr_ptr_type->pointee.comptime_val;
		break;
	case EXPR_SLICE_TYPE:
		Expr_Validate(&e->as.expr_slice_type->item, ctx);
		if(e->as.expr_slice_type->item.type.kind != TYPE_TYPE)
			fatal(e->loc, "the item type is not a type");
		e->type.kind = TYPE_TYPE;
		e->comptime_val = safe_ptr(malloc(sizeof(Type)));
		*(Type*)e->comptime_val = (Type) {
			.kind = TYPE_SLICE,
			.as.type_slice = safe_ptr(malloc(sizeof(Slice)))
		};
		((Type*)e->comptime_val)->as.type_slice->item = *(Type*)e->as.expr_slice_type->item.comptime_val;
		break;
	case EXPR_FN_TYPE:
		Expr_Validate(&e->as.expr_fn_type->ret_type, ctx);
		if(e->as.expr_fn_type->ret_type.type.kind != TYPE_TYPE)
			fatal(e->loc, "return type is not a type");
		e->type.kind = TYPE_TYPE;
		e->comptime_val = safe_ptr(malloc(sizeof(Type)));
		*(Type*)e->comptime_val = (Type) {
			.kind = TYPE_FN,
			.as.type_fn = safe_ptr(malloc(sizeof(Fn)))
		};

		{
			Type *args = malloc(sizeof(*args) * e->as.expr_fn_type->args_count);
			for(u32 i = 0; i < e->as.expr_fn_type->args_count; i++) {
				Expr_Validate(&e->as.expr_fn_type->args[i], ctx);
				args[i] = *(Type*)safe_ptr(e->as.expr_fn_type->args[i].comptime_val);
			}

			*((Type*)e->comptime_val)->as.type_fn = (Fn) {
				.ret_type = *(Type*)e->as.expr_fn_type->ret_type.comptime_val,
				.args_count = e->as.expr_fn_type->args_count,
				.args = args
			};
		}
		break;
	case EXPR_CALL:
		Expr_Validate(&e->as.expr_call->fn, ctx);
		if(e->as.expr_call->fn.type.kind != TYPE_FN)
			fatal(e->loc, "attempted to call something that is not a function");
		e->type = e->as.expr_call->fn.type.as.type_fn->ret_type;

		if(e->as.expr_call->args_count != e->as.expr_call->fn.type.as.type_fn->args_count)
			fatal(e->loc, "invalid amount of arguments");

		for(u32 i = 0; i < e->as.expr_call->args_count; i++) {
			Expr_Validate(&e->as.expr_call->args[i], ctx);
			Type *arg_type = &e->as.expr_call->fn.type.as.type_fn->args[i];
			if(Type_Compare(&e->as.expr_call->args[i].type, arg_type)) {
				Type_ValidateCast(e->as.expr_call->args[i].loc, &e->as.expr_call->args[i].type, arg_type);
				e->as.expr_call->args[i].cast = true;
				e->as.expr_call->args[i].cast_to = *arg_type;
			}
		}

		break;
	case EXPR_BIN_OP: {
		Expr_Validate(&e->as.expr_bin_op->lhs, ctx);
		Expr_Validate(&e->as.expr_bin_op->rhs, ctx);
		Type lhs = e->as.expr_bin_op->lhs.type;
		Type rhs = e->as.expr_bin_op->rhs.type;
		static_assert(COUNT_BIN_OP == 12);
		switch(e->as.expr_bin_op->kind) {
		case BIN_OP_CAST:
			if(e->as.expr_bin_op->rhs.type.kind != TYPE_TYPE)
				fatal(e->loc, "right hand side of a cast must be a type");
			Type_ValidateCast(e->loc, &e->as.expr_bin_op->lhs.type, safe_ptr(e->as.expr_bin_op->rhs.comptime_val));
			e->type = *safe_ptr((Type*)e->as.expr_bin_op->rhs.comptime_val);
			break;
		case BIN_OP_MUL:
		case BIN_OP_DIV:
		case BIN_OP_MOD:
		case BIN_OP_ADD:
		case BIN_OP_SUB:
			if(lhs.kind != TYPE_INT)
				fatal(e->loc, "left hand side of an arithmetic operation is not an integer");
			if(rhs.kind != TYPE_INT)
				fatal(e->loc, "right hand side of an arithmetic operation is not an integer");

			e->type = (Type) {
				.kind = TYPE_INT,
				.as.type_int = {
					.sign = lhs.as.type_int.sign || rhs.as.type_int.sign,
					.bits = lhs.as.type_int.bits > rhs.as.type_int.bits ? lhs.as.type_int.bits : rhs.as.type_int.bits
				}
			};

			e->as.expr_bin_op->lhs.cast = Type_Compare(&e->type, &e->as.expr_bin_op->lhs.type);
			e->as.expr_bin_op->rhs.cast = Type_Compare(&e->type, &e->as.expr_bin_op->rhs.type);
			e->as.expr_bin_op->lhs.cast_to = e->type;
			e->as.expr_bin_op->rhs.cast_to = e->type;

			break;
		case BIN_OP_EQ:
		case BIN_OP_NE:
		case BIN_OP_GE:
		case BIN_OP_GT:
		case BIN_OP_LE:
		case BIN_OP_LT:
			if(lhs.kind != TYPE_INT)
				fatal(e->loc, "left hand side of a comparison is not an integer");
			if(rhs.kind != TYPE_INT)
				fatal(e->loc, "right hand side of a comparison is not an integer");

			{
				Type type =  {
					.kind = TYPE_INT,
					.as.type_int = {
						.sign = lhs.as.type_int.sign || rhs.as.type_int.sign,
						.bits = lhs.as.type_int.bits > rhs.as.type_int.bits ? lhs.as.type_int.bits : rhs.as.type_int.bits
					}
				};

				e->as.expr_bin_op->lhs.cast = Type_Compare(&type, &e->as.expr_bin_op->lhs.type);
				e->as.expr_bin_op->rhs.cast = Type_Compare(&type, &e->as.expr_bin_op->rhs.type);
				e->as.expr_bin_op->lhs.cast_to = type;
				e->as.expr_bin_op->rhs.cast_to = type;
			}

			e->type.kind = TYPE_BOOL;

			break;
		default: unreachable();
		}
		break; }
	default:
		unreachable();
	}
}

void Stmt_Validate(Stmt *s, ValidationCtx *ctx)
{
	if(ctx->in_func && ctx->returned)
		fatal(s->loc, "unreachable code");

	static_assert(COUNT_STMT == 7);
	switch(s->kind) {
	case STMT_FN_DECL:
		if(ctx->in_func)
			fatal(s->loc, "functions cannot be nested");
		if(s->as.stmt_fn_decl.has_ret_type) {
			Expr_Validate(&s->as.stmt_fn_decl.ret_type, ctx);
			if(s->as.stmt_fn_decl.ret_type.type.kind != TYPE_TYPE)
				fatal(s->as.stmt_fn_decl.ret_type.loc, "the return type is not a type");
			ctx->ret_type = *safe_ptr((Type*)s->as.stmt_fn_decl.ret_type.comptime_val);
		} else {
			ctx->ret_type.kind = TYPE_VOID;
		}

		s->as.stmt_fn_decl.fn_type = (Type) {
			.kind = TYPE_FN,
			.as.type_fn = safe_ptr(malloc(sizeof(Fn)))
		};

		{
			Type *args = safe_ptr(malloc(sizeof(*args) * s->as.stmt_fn_decl.args_count));
			for(u32 i = 0; i < s->as.stmt_fn_decl.args_count; i++) {
				Expr_Validate(&s->as.stmt_fn_decl.args[i].type, ctx);
				if(s->as.stmt_fn_decl.args[i].type.type.kind != TYPE_TYPE)
					fatal(s->as.stmt_fn_decl.args[i].loc, "argument's type is not a type");
				args[i] = *(Type*)s->as.stmt_fn_decl.args[i].type.comptime_val;
			}

			*s->as.stmt_fn_decl.fn_type.as.type_fn = (Fn) {
				.ret_type = ctx->ret_type,
				.args_count = s->as.stmt_fn_decl.args_count,
				.args = args
			};


			Scope_DefineInValidation(&ctx->scope, s->loc, s->as.stmt_fn_decl.name, s->as.stmt_fn_decl.fn_type, NULL, false, true);
		}

		if(s->as.stmt_fn_decl.external)
			break;

		u32 scope_backup = ctx->scope.count;

		for(u32 i = 0; i < s->as.stmt_fn_decl.args_count; i++) {
			FnArgDecl arg = s->as.stmt_fn_decl.args[i];
			Scope_DefineInValidation(&ctx->scope, arg.loc, arg.name, *(Type*)arg.type.comptime_val, NULL, true, true);
		}

		ctx->in_func = true;
		ctx->returned = false;
		Stmt_ValidateMany(s->as.stmt_fn_decl.body, s->as.stmt_fn_decl.body_len, ctx);
		if(!ctx->returned && ctx->ret_type.kind != TYPE_VOID)
			fatal(s->loc, "function doesn't return");
		s->as.stmt_fn_decl.implicit_return = !ctx->returned;
		ctx->in_func = false;
		ctx->scope.count = scope_backup;
		break;
	case STMT_RETURN:
		if(!ctx->in_func)
			fatal(s->loc, "returns cannot be outside functions");
		if(ctx->ret_type.kind == TYPE_VOID && s->as.stmt_return.has_value)
			fatal(s->loc, "function with void return type cannot return a value");
		if(ctx->ret_type.kind != TYPE_VOID && !s->as.stmt_return.has_value)
			fatal(s->loc, "function with non-void return type must return a value");

		if(s->as.stmt_return.has_value) {
			Expr_Validate(&s->as.stmt_return.value, ctx);
			if(Type_Compare(&s->as.stmt_return.value.type, &ctx->ret_type)) {
				Type_ValidateCast(s->loc, &s->as.stmt_return.value.type, &ctx->ret_type);
				s->as.stmt_return.value.cast = true;
				s->as.stmt_return.value.cast_to = ctx->ret_type;
			} else {
				s->as.stmt_return.value.cast = false;
			}
		}

		ctx->returned = true;
		break;
	case STMT_EXPR:
		if(!ctx->in_func)
			fatal(s->loc, "expression statements cannot be outside functions");
		Expr_Validate(&s->as.stmt_expr, ctx);
		break;
	case STMT_VAR:
		Expr_Validate(&s->as.stmt_var.val, ctx);
		Scope_DefineInValidation(&ctx->scope, s->loc, s->as.stmt_var.name, s->as.stmt_var.val.type, s->as.stmt_var.val.comptime_val, true, s->as.stmt_var.constant);
		break;
	case STMT_ASSIGN:
		if(!ctx->in_func)
			fatal(s->loc, "assignments cannot be outside functions");
		Expr_Validate(&s->as.stmt_assign.lhs, ctx);
		Expr_Validate(&s->as.stmt_assign.rhs, ctx);

		if(s->as.stmt_assign.lhs.constant)
			fatal(s->loc, "cannot assign to a constant");

		if(Type_Compare(&s->as.stmt_assign.lhs.type, &s->as.stmt_assign.rhs.type)) {
			Type_ValidateCast(s->loc, &s->as.stmt_assign.rhs.type, &s->as.stmt_assign.lhs.type);
			s->as.stmt_assign.rhs.cast = true;
			s->as.stmt_assign.rhs.cast_to = s->as.stmt_assign.lhs.type;
		}

		break;
	case STMT_IF: {
		Expr_Validate(&s->as.stmt_if.cond, ctx);
		if(s->as.stmt_if.cond.type.kind != TYPE_BOOL)
			fatal(s->loc, "condition is not a boolean");

		u32 scope_backup = ctx->scope.count;
		Stmt_ValidateMany(s->as.stmt_if.body, s->as.stmt_if.body_len, ctx);
		ctx->scope.count = scope_backup;
		bool body_returned = ctx->returned;
		ctx->returned = false;

		scope_backup = ctx->scope.count;
		Stmt_ValidateMany(s->as.stmt_if.else_body, s->as.stmt_if.else_len, ctx);
		ctx->scope.count = scope_backup;
		ctx->returned &= body_returned;
		break; }
	case STMT_WHILE: {
		Expr_Validate(&s->as.stmt_while.cond, ctx);
		if(s->as.stmt_while.cond.type.kind != TYPE_BOOL)
			fatal(s->loc, "condition is not a boolean");

		u32 scope_backup = ctx->scope.count;
		Stmt_ValidateMany(s->as.stmt_while.body, s->as.stmt_while.body_len, ctx);
		ctx->scope.count = scope_backup;
		ctx->returned = false;
		break; }
	default:
		unreachable();
	}
}

void Stmt_ValidateMany(Stmt *s, u32 len, ValidationCtx *ctx)
{
	for(u32 i = 0; i < len; i++)
		Stmt_Validate(&s[i], ctx);
}

typedef struct {
	LLVMModuleRef mod;
	LLVMBuilderRef bld; // might be NULL
	Scope scope;
} CodegenCtx;

LLVMTypeRef Type_Codegen(Type *t);
void Stmt_CodegenMany(Stmt *s, u32 len, CodegenCtx *ctx);

LLVMTypeRef Type_Fn_Codegen(Type *t)
{
	LLVMTypeRef *args = safe_ptr(malloc(sizeof(*args) * t->as.type_fn->args_count));
	for(u32 i = 0; i < t->as.type_fn->args_count; i++)
		args[i] = Type_Codegen(&t->as.type_fn->args[i]);
	return LLVMFunctionType(Type_Codegen(&t->as.type_fn->ret_type), args, t->as.type_fn->args_count, false);
}

LLVMTypeRef Type_Codegen(Type *t)
{
	static_assert(COUNT_TYPE == 7);
	switch(t->kind) {
	case TYPE_VOID:
		return LLVMVoidType();
	case TYPE_BOOL:
		return LLVMIntType(1);
	case TYPE_INT:
		return LLVMIntType(t->as.type_int.bits);
	case TYPE_PTR:
		return LLVMPointerType(Type_Codegen(&t->as.type_ptr->pointee), 0);
	case TYPE_SLICE: {
		LLVMTypeRef *slice = malloc(sizeof(*slice) * 2);
		slice[0] = Type_Codegen(&t->as.type_slice->item);
		slice[1] = LLVMIntType(64); // TODO: usize here
		return LLVMStructType(slice, 2, false); }
	case TYPE_FN:
		return LLVMPointerType(Type_Fn_Codegen(t), 0);
	default:
		unreachable();
	}
}

LLVMValueRef Type_CodegenCast(LLVMValueRef val, Type *from, Type *to, CodegenCtx *ctx)
{
	static_assert(COUNT_TYPE == 7);
	switch(to->kind) {
	case TYPE_INT:
		assert(from->kind == TYPE_INT);
		if(to->as.type_int.bits < from->as.type_int.bits)
			return LLVMBuildTrunc(safe_ptr(ctx->bld), val, Type_Codegen(to), "");
		if(to->as.type_int.sign && from->as.type_int.sign)
			return LLVMBuildSExt(safe_ptr(ctx->bld), val, Type_Codegen(to), "");
		return LLVMBuildZExt(safe_ptr(ctx->bld), val, Type_Codegen(to), "");
	default:
		unreachable();
	}
}

LLVMValueRef Expr_Codegen(Expr *e, CodegenCtx *ctx);

LLVMValueRef Expr_CodegenLValue(Expr *e, CodegenCtx *ctx)
{
	assert(e->type.kind != TYPE_TYPE);
	assert(e->is_lvalue);

	static_assert(COUNT_EXPR == 9);
	switch(e->kind) {
	case EXPR_IDENTIFIER:
		return Scope_Find(&ctx->scope, e->loc, e->as.expr_identifier).value.codegen;
	default:
		unreachable();
	}
}

LLVMValueRef Expr_CodegenNoCast(Expr *e, CodegenCtx *ctx)
{
	assert(e->type.kind != TYPE_TYPE);

	static_assert(COUNT_EXPR == 9);
	switch(e->kind) {
	case EXPR_INT_LIT:
		return LLVMConstInt(LLVMIntType(64), e->as.expr_int_lit, false);
	case EXPR_BOOL_LIT:
		return LLVMConstInt(LLVMIntType(1), e->as.expr_bool_lit, false);
	case EXPR_STR_LIT: {
		u64 len = strlen(e->as.expr_str_lit);
		LLVMValueRef chars = LLVMAddGlobal(ctx->mod, LLVMArrayType(LLVMIntType(8), len), "str");
		LLVMSetInitializer(chars, LLVMConstString(e->as.expr_str_lit, len, true));

		LLVMValueRef *vals = malloc(2 * sizeof(*vals));
		vals[0] = chars;
		vals[1] = LLVMConstInt(LLVMIntType(64), len, false);

		LLVMValueRef str = LLVMConstStruct(vals, 2, false);
		return str; }
	case EXPR_IDENTIFIER: {
		LLVMValueRef val = Scope_Find(&ctx->scope, e->loc, e->as.expr_identifier).value.codegen;
		if(e->is_lvalue)
			val = LLVMBuildLoad2(safe_ptr(ctx->bld), Type_Codegen(&e->type), val, "");
		return val; }
	case EXPR_CALL: {
		LLVMValueRef fn = Expr_Codegen(&e->as.expr_call->fn, ctx);

		LLVMValueRef *args = safe_ptr(malloc(sizeof(*args) * e->as.expr_call->args_count));
		for(u32 i = 0; i < e->as.expr_call->args_count; i++) {
			args[i] = Expr_Codegen(&e->as.expr_call->args[i], ctx);
		}
		return LLVMBuildCall2(safe_ptr(ctx->bld), Type_Fn_Codegen(&e->as.expr_call->fn.type), fn, args, e->as.expr_call->args_count, "");
		break; }
	case EXPR_BIN_OP: {
		LLVMValueRef lhs = Expr_Codegen(&e->as.expr_bin_op->lhs, ctx);
		LLVMValueRef rhs = e->as.expr_bin_op->kind != BIN_OP_CAST
			? Expr_Codegen(&e->as.expr_bin_op->rhs, ctx)
			: NULL;

		static_assert(COUNT_BIN_OP == 12);
		switch(e->as.expr_bin_op->kind) {
		case BIN_OP_CAST:
			return lhs; // cast is done with Expr_Codegen
		case BIN_OP_MUL:
			return LLVMBuildMul(safe_ptr(ctx->bld), lhs, rhs, "");
		case BIN_OP_DIV:
			return e->type.as.type_int.sign
				? LLVMBuildSDiv(safe_ptr(ctx->bld), lhs, rhs, "")
				: LLVMBuildUDiv(safe_ptr(ctx->bld), lhs, rhs, "");
		case BIN_OP_MOD:
			return e->type.as.type_int.sign
				? LLVMBuildSRem(safe_ptr(ctx->bld), lhs, rhs, "")
				: LLVMBuildURem(safe_ptr(ctx->bld), lhs, rhs, "");
		case BIN_OP_SUB:
			return LLVMBuildSub(safe_ptr(ctx->bld), lhs, rhs, "");
		case BIN_OP_ADD:
			return LLVMBuildAdd(safe_ptr(ctx->bld), lhs, rhs, "");
		case BIN_OP_EQ:
			return LLVMBuildICmp(safe_ptr(ctx->bld), LLVMIntEQ, lhs, rhs, "");
		case BIN_OP_NE:
			return LLVMBuildICmp(safe_ptr(ctx->bld), LLVMIntNE, lhs, rhs, "");
		case BIN_OP_GE:
			return e->as.expr_bin_op->lhs.cast_to.as.type_int.sign
				? LLVMBuildICmp(safe_ptr(ctx->bld), LLVMIntSGE, lhs, rhs, "")
				: LLVMBuildICmp(safe_ptr(ctx->bld), LLVMIntUGE, lhs, rhs, "");
		case BIN_OP_GT:
			return e->as.expr_bin_op->lhs.cast_to.as.type_int.sign
				? LLVMBuildICmp(safe_ptr(ctx->bld), LLVMIntSGT, lhs, rhs, "")
				: LLVMBuildICmp(safe_ptr(ctx->bld), LLVMIntUGT, lhs, rhs, "");
		case BIN_OP_LE:
			return e->as.expr_bin_op->lhs.cast_to.as.type_int.sign
				? LLVMBuildICmp(safe_ptr(ctx->bld), LLVMIntSLE, lhs, rhs, "")
				: LLVMBuildICmp(safe_ptr(ctx->bld), LLVMIntULE, lhs, rhs, "");
		case BIN_OP_LT:
			return e->as.expr_bin_op->lhs.cast_to.as.type_int.sign
				? LLVMBuildICmp(safe_ptr(ctx->bld), LLVMIntSLT, lhs, rhs, "")
				: LLVMBuildICmp(safe_ptr(ctx->bld), LLVMIntULT, lhs, rhs, "");
		default:
			unreachable();
		}
		break; }
	default:
		unreachable();
	}
}

LLVMValueRef Expr_Codegen(Expr *e, CodegenCtx *ctx)
{
	LLVMValueRef val = Expr_CodegenNoCast(e, ctx);
	if(e->cast)
		val = Type_CodegenCast(val, &e->type, &e->cast_to, ctx);
	return val;
}

void Stmt_FnDecl_Codegen(Stmt *s, CodegenCtx *ctx)
{
	LLVMTypeRef fn_type = Type_Fn_Codegen(&s->as.stmt_fn_decl.fn_type);
	LLVMValueRef fn = LLVMAddFunction(ctx->mod, s->as.stmt_fn_decl.name, fn_type);
	Scope_DefineInCodegen(&ctx->scope, s->loc, s->as.stmt_fn_decl.name, fn);

	if(s->as.stmt_fn_decl.external)
		return;

	u32 scope_backup = ctx->scope.count;

	LLVMBasicBlockRef entry = LLVMAppendBasicBlock(fn, "entry");

	ctx->bld = LLVMCreateBuilder();
	LLVMPositionBuilderAtEnd(ctx->bld, entry);

	for(u32 i = 0; i < s->as.stmt_fn_decl.args_count; i++) {
		FnArgDecl arg = s->as.stmt_fn_decl.args[i];
		LLVMValueRef ref = LLVMBuildAlloca(ctx->bld, Type_Codegen(arg.type.comptime_val), "arg");
		LLVMBuildStore(ctx->bld, LLVMGetParam(fn, i), ref);
		Scope_DefineInCodegen(&ctx->scope, arg.loc, arg.name, ref);
	}

	Stmt_CodegenMany(s->as.stmt_fn_decl.body, s->as.stmt_fn_decl.body_len, ctx);

	if(s->as.stmt_fn_decl.implicit_return)
		LLVMBuildRetVoid(safe_ptr(ctx->bld));

	ctx->bld = NULL;
	ctx->scope.count = scope_backup;
}

void Stmt_Return_Codegen(Stmt *s, CodegenCtx *ctx)
{
	if(s->as.stmt_return.has_value) {
		LLVMValueRef val = Expr_Codegen(&s->as.stmt_return.value, ctx);
		LLVMBuildRet(safe_ptr(ctx->bld), val);
	} else {
		LLVMBuildRetVoid(safe_ptr(ctx->bld));
	}
}

void Stmt_Var_Codegen(Stmt *s, CodegenCtx *ctx)
{
	LLVMValueRef val = Expr_Codegen(&s->as.stmt_var.val, ctx);
	LLVMTypeRef type = Type_Codegen(&s->as.stmt_var.val.type);

	LLVMValueRef ref;
	if(ctx->bld) {
		ref = LLVMBuildAlloca(safe_ptr(ctx->bld), type, "");
		LLVMBuildStore(safe_ptr(ctx->bld), val, ref);
	} else {
		ref = LLVMAddGlobal(ctx->mod, type, "const");
		LLVMSetInitializer(ref, val);
	}

	Scope_DefineInCodegen(&ctx->scope, s->loc, s->as.stmt_var.name, ref);
}

void Stmt_Assign_Codegen(Stmt *s, CodegenCtx *ctx)
{
	LLVMValueRef lhs = Expr_CodegenLValue(&s->as.stmt_assign.lhs, ctx);
	LLVMValueRef rhs = Expr_Codegen(&s->as.stmt_assign.rhs, ctx);

	LLVMBuildStore(safe_ptr(ctx->bld), rhs, lhs);
}

void Stmt_If_Codegen(Stmt *s, CodegenCtx *ctx)
{
	// save stuff
	LLVMBasicBlockRef entry = LLVMGetInsertBlock(safe_ptr(ctx->bld));
	LLVMValueRef fn = LLVMGetBasicBlockParent(entry);

	// create the condition
	LLVMValueRef cond = Expr_Codegen(&s->as.stmt_if.cond, ctx);

	// create the body branch
	LLVMBasicBlockRef br_body = LLVMAppendBasicBlock(fn, "if_body");
	LLVMPositionBuilderAtEnd(ctx->bld, br_body);
	u32 scope_backup = ctx->scope.count;
	Stmt_CodegenMany(s->as.stmt_if.body, s->as.stmt_if.body_len, ctx);
	ctx->scope.count = scope_backup;
	LLVMBasicBlockRef br_body_end = LLVMGetInsertBlock(ctx->bld);

	// create the else branch
	LLVMBasicBlockRef br_else = LLVMAppendBasicBlock(fn, "if_else");
	LLVMPositionBuilderAtEnd(ctx->bld, br_else);
	scope_backup = ctx->scope.count;
	Stmt_CodegenMany(s->as.stmt_if.else_body, s->as.stmt_if.else_len, ctx);
	ctx->scope.count = scope_backup;

	// create the end branch
	LLVMBasicBlockRef br_end = LLVMAppendBasicBlock(fn, "if_end");

	// link the end of the else to the end
	if(!LLVMGetBasicBlockTerminator(LLVMGetInsertBlock(ctx->bld)))
		LLVMBuildBr(ctx->bld, br_end);

	// link the end of the body to the end
	LLVMPositionBuilderAtEnd(ctx->bld, br_body_end);
	if(!LLVMGetBasicBlockTerminator(LLVMGetInsertBlock(ctx->bld)))
		LLVMBuildBr(ctx->bld, br_end);

	// create the conditional branch
	LLVMPositionBuilderAtEnd(ctx->bld, entry);
	assert(!LLVMGetBasicBlockTerminator(LLVMGetInsertBlock(ctx->bld)));
	LLVMBuildCondBr(ctx->bld, cond, br_body, br_else);

	LLVMPositionBuilderAtEnd(ctx->bld, br_end);
}

void Stmt_While_Codegen(Stmt *s, CodegenCtx *ctx)
{
	// save stuff
	LLVMBasicBlockRef entry = LLVMGetInsertBlock(safe_ptr(ctx->bld));
	LLVMValueRef fn = LLVMGetBasicBlockParent(entry);

	// create the condition
	LLVMBasicBlockRef br_cond = LLVMAppendBasicBlock(fn, "while_cond");
	assert(!LLVMGetBasicBlockTerminator(LLVMGetInsertBlock(ctx->bld)));
	LLVMBuildBr(ctx->bld, br_cond);
	LLVMPositionBuilderAtEnd(ctx->bld, br_cond);
	LLVMValueRef cond = Expr_Codegen(&s->as.stmt_while.cond, ctx);
	LLVMBasicBlockRef br_cond_end = LLVMGetInsertBlock(ctx->bld);

	// create the body branch
	LLVMBasicBlockRef br_body = LLVMAppendBasicBlock(fn, "while_body");
	LLVMPositionBuilderAtEnd(ctx->bld, br_body);
	u32 scope_backup = ctx->scope.count;
	Stmt_CodegenMany(s->as.stmt_while.body, s->as.stmt_while.body_len, ctx);
	ctx->scope.count = scope_backup;

	// link the end of the body to the condition
	if(!LLVMGetBasicBlockTerminator(LLVMGetInsertBlock(ctx->bld)))
		LLVMBuildBr(ctx->bld, br_cond);

	// create the end branch
	LLVMBasicBlockRef br_end = LLVMAppendBasicBlock(fn, "while_end");

	// create the conditional branch
	LLVMPositionBuilderAtEnd(ctx->bld, br_cond_end);
	assert(!LLVMGetBasicBlockTerminator(LLVMGetInsertBlock(ctx->bld)));
	LLVMBuildCondBr(ctx->bld, cond, br_body, br_end);

	LLVMPositionBuilderAtEnd(ctx->bld, br_end);
}

void Stmt_Codegen(Stmt *s, CodegenCtx *ctx)
{
	static_assert(COUNT_STMT == 7);
	switch(s->kind) {
	case STMT_FN_DECL:
		Stmt_FnDecl_Codegen(s, ctx);
		break;
	case STMT_RETURN:
		Stmt_Return_Codegen(s, ctx);
		break;
	case STMT_EXPR:
		Expr_Codegen(&s->as.stmt_expr, ctx);
		break;
	case STMT_VAR:
		Stmt_Var_Codegen(s, ctx);
		break;
	case STMT_ASSIGN:
		Stmt_Assign_Codegen(s, ctx);
		break;
	case STMT_IF:
		Stmt_If_Codegen(s, ctx);
		break;
	case STMT_WHILE:
		Stmt_While_Codegen(s, ctx);
		break;
	default:
		unreachable();
	}
}

void Stmt_CodegenMany(Stmt *s, u32 len, CodegenCtx *ctx)
{
	for(u32 i = 0; i < len; i++)
		Stmt_Codegen(&s[i], ctx);
}

Lexer lexer;

void close_lexer(void)
{
	Lexer_Close(&lexer);
}

int main(void)
{
	char *input_file = "main.caro";
	lexer = Lexer_Init(input_file);
	atexit(close_lexer);

	u32 ast_len;
	Stmt *ast = Stmt_ParseMany(&lexer, &ast_len);
	if(Lexer_Peek(&lexer) != TOKEN_EOF)
		Lexer_Unexpected(&lexer, "a statement");

	ValidationCtx val_ctx = {0};

	Type type_void = { .kind = TYPE_VOID };
	Type type_bool = { .kind = TYPE_BOOL };
	Type type_type = { .kind = TYPE_TYPE };
	Type type_u8   = { .kind = TYPE_INT, .as.type_int = { false, 8  } };
	Type type_i8   = { .kind = TYPE_INT, .as.type_int = { true , 8  } };
	Type type_u16  = { .kind = TYPE_INT, .as.type_int = { false, 16 } };
	Type type_i16  = { .kind = TYPE_INT, .as.type_int = { true , 16 } };
	Type type_u32  = { .kind = TYPE_INT, .as.type_int = { false, 32 } };
	Type type_i32  = { .kind = TYPE_INT, .as.type_int = { true , 32 } };
	Type type_u64  = { .kind = TYPE_INT, .as.type_int = { false, 64 } };
	Type type_i64  = { .kind = TYPE_INT, .as.type_int = { true , 64 } };
	Scope_DefineInValidation(&val_ctx.scope, ((Loc) { "<builtin>", 0, 0 }), "void", type_type, &type_void, false, true);
	Scope_DefineInValidation(&val_ctx.scope, ((Loc) { "<builtin>", 0, 0 }), "bool", type_type, &type_bool, false, true);
	Scope_DefineInValidation(&val_ctx.scope, ((Loc) { "<builtin>", 0, 0 }), "type", type_type, &type_type, false, true);
	Scope_DefineInValidation(&val_ctx.scope, ((Loc) { "<builtin>", 0, 0 }), "u8"  , type_type, &type_u8  , false, true);
	Scope_DefineInValidation(&val_ctx.scope, ((Loc) { "<builtin>", 0, 0 }), "i8"  , type_type, &type_i8  , false, true);
	Scope_DefineInValidation(&val_ctx.scope, ((Loc) { "<builtin>", 0, 0 }), "u16" , type_type, &type_u16 , false, true);
	Scope_DefineInValidation(&val_ctx.scope, ((Loc) { "<builtin>", 0, 0 }), "i16" , type_type, &type_i16 , false, true);
	Scope_DefineInValidation(&val_ctx.scope, ((Loc) { "<builtin>", 0, 0 }), "u32" , type_type, &type_u32 , false, true);
	Scope_DefineInValidation(&val_ctx.scope, ((Loc) { "<builtin>", 0, 0 }), "i32" , type_type, &type_i32 , false, true);
	Scope_DefineInValidation(&val_ctx.scope, ((Loc) { "<builtin>", 0, 0 }), "u64" , type_type, &type_u64 , false, true);
	Scope_DefineInValidation(&val_ctx.scope, ((Loc) { "<builtin>", 0, 0 }), "i64" , type_type, &type_i64 , false, true);

	Stmt_ValidateMany(ast, ast_len, &val_ctx);

	CodegenCtx cg_ctx = {0};
	cg_ctx.mod = LLVMModuleCreateWithName(input_file);
	Stmt_CodegenMany(ast, ast_len, &cg_ctx);
	LLVMDumpModule(cg_ctx.mod);
}
