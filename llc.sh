set -xe
gcc -Werror -Wall -Wextra -pedantic -ggdb caro.c -o caro -lLLVM
./caro 2>&1 >/dev/null | llc --filetype=obj > main.o
gcc main.o carostd.c -o main
./main
